# Changelog

## [v1.1](https://gitlab.com/sinclear/dcdp/-/tags/v1.1) - 02.10.2019
- Der erste Bugfix, welch ein Moment...
- Und dann auch noch gleich 2 neue Felder, eins für Empfehlungen und ein anderes für einen sekundären Link (z.B. wenn ein Deal für ein Spiel auf 2 Plattformen gleichzeitig zutrifft oder es mehrere Versionen gibt)

## [v1.0](https://gitlab.com/sinclear/dcdp/-/tags/v1.0) - 01.10.2019
- Hier beginnt die Reise.
- Erste funktionierende Version mit grundlegenden Features.