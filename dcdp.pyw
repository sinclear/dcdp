# import appJar library
from appJar import gui
import http.client
from urllib.parse import urlencode
import json

# get the key
with open("key.txt", "r") as file:
    webhookKey = file.read().replace("\n", "")

def press(button):
    if button == "Beenden":
        app.stop()
    elif button == "Absenden":
        webhookKey = app.getEntry("WebHook Key")
        here = app.getCheckBox("@here")
        begruessung = app.getTextArea("Begrüßung")
        titel = app.getEntry("Titel")
        plattform = app.getEntry("Plattform")
        rabatt = app.getEntry("Rabatt (%)")
        preis = app.getEntry("Preis (€)")
        bis = app.getEntry("Tage verbleibend")
        empfehlung = app.getEntry("Empfohlen von")
        link1 = app.getEntry("Link 1")
        link2 = app.getEntry("Link 2")

        # put all the fields together into one biiiiig string (if they exist)
        # deal = begruessung + "\\n:label: **" + titel + "**\\n:globe_with_meridians: " + plattform + "\\n:anger: -" + rabatt + "%\\n:euro: " + preis + "€\\n:link: " + link
        d = []
        if here == True:
            d.append("@here ")
        if begruessung != "":
            d.append(begruessung + "\\n")
        if titel != "":
            d.append(":label: **" + titel + "**\\n")
        if plattform != "":
            d.append(":globe_with_meridians: " + plattform + "\\n")
        if rabatt != "":
            d.append(":anger: -" + rabatt + "%\\n")
        if preis != "":
            d.append(":euro: " + preis + "€\\n")
        if bis != "":
            d.append(":alarm_clock: Noch " + bis + " Tage\\n")
        if empfehlung != "":
            d.append(":busts_in_silhouette: Empfohlen von " + empfehlung + "\\n")
        if link1 != "":
            d.append(":link: " + link1 + "\\n")
        if link2 != "":
            d.append(":link: " + link2 + "\\n")

        deal = ''.join(d)
        payload = '{"username": "DCDP", "content": "' + deal + '"}'

        # beam it up, scotty!
        conn = http.client.HTTPSConnection("discordapp.com")
        headers = {
            'cookie': "__cfduid=d6dd168ab36da376115a8d3dc0066e0ec1567621924",
            'content-type': "application/json"
        }
        conn.request("POST", "/api/webhooks/" + webhookKey, payload.encode("utf-8"), headers)
        res = conn.getresponse()
        data = res.read()
        print(data.decode("utf-8"))

# create GUI variable
app = gui("DCDP", "910x520")
app.setLocation("CENTER")

# greeting label
app.addLabel("topLabel", "Willkommen bei Discord Deals Poster     (v1.1)")
app.setLabelBg("topLabel", "green")

# input fields
app.addLabelEntry("WebHook Key")
app.setEntry("WebHook Key", webhookKey)
app.addCheckBox("@here")
app.addLabel("Begrüßungs-Text                                                                            (Umbrüche nur via \\n, KEIN ENTER!  →  z.B. 'Hier ist ein\\ntoller Deal')")
app.addTextArea("Begrüßung")
app.setTextArea("Begrüßung", "Hier ist ein toller neuer Deal")
app.addLabelEntry("Titel")
app.addLabelEntry("Plattform")
app.addLabelEntry("Rabatt (%)")
app.addLabelEntry("Preis (€)")
app.addLabelEntry("Tage verbleibend")
app.addLabelEntry("Empfohlen von")
app.addLabelEntry("Link 1")
app.addLabelEntry("Link 2")

# add the buttons below the form
app.addButtons(["Absenden", "Beenden"], press)

# cursor direkt ins erste Feld
app.setFocus("WebHook Key")

# start the GUI
app.go()