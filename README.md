# Discord Deals Poster

⚠️ **Eine komplett überarbeitete und in Java neu programmierte Version von DCDP ist ab sofort auf GitHub verfügbar: https://github.com/sinclearclan/dcdpx**

### Schnelle Links

- [Downloads](https://gitlab.com/sinclear/dcdp/-/tags)
- [Changelog](https://gitlab.com/sinclear/dcdp/blob/master/CHANGELOG.md)


## Über _Discord Deals Poster (DCDP)_

_Discord Deals Poster_ ist ein [GUI](https://de.wikipedia.org/wiki/Grafische_Benutzeroberfl%C3%A4che)-Programm, mit dem man ganz einfach über einen [WebHook](https://de.wikipedia.org/wiki/WebHooks) Deals an einen [Discord](https://de.wikipedia.org/wiki/Discord_(Software))-Server schicken kann. Diese Deals sind dabei so formatiert, wie auf unserem Discord-Server [_Sinclear_](https://sinclear.de). Hier ein Beispiel, wie ein geposteter Deal in Discord aussieht:

![Beispiel: Discord](https://gitlab.com/sinclear/dcdp/raw/master/images/example_discord.png)

## So wird's installiert

1. Bevor du _DCDP_ verwenden kannst, musst du [Python3 von der offiziellen Website installieren](https://www.python.org/downloads/). Wenn du ein 64bit-System hast, kannst du dir auch einen [Installer für 64bit](https://www.python.org/downloads/windows/) downloaden (unter Stable Releases die Windows x86-64 executable installer oder web-based installer).
2. Bei der Installation von Python3 solltest du auf jeden Fall die das Häkchen bei "Add Python X.X to PATH", das spart dir Arbeit und Frust.

![Installation: Python3](https://gitlab.com/sinclear/dcdp/raw/master/images/python_installation_PATH.png)

3. Lade dir eine aktuelle Version von _DCDP_ herunter. Links zu den Downloads aller Versionen findest du oben.
4. Entpacke die Dateien an einen Ort. Komplett egal wo. Du musst es nur wiederfinden. Desktop ist auch ok.
5. Finde in Discord die URL deines WebHooks. Wenn du noch keinen WebHook hast, dann solltest du spätestens jetzt einen erstellen. Kopiere die URL.
6. Erstelle eine Datei namens _key.txt_ und gib dort die URL deines WebHooks ein.
7. Anschließend löschst du von dieser URL den Anfang weg, genauer: "https://discordapp.com/api/webhooks/", sodass nur noch das Buchstaben- und Zahlen-Wirrwarr übrig bleibt und speichere die Datei. Das sollte dann am Ende zum Beispiel so aussehen:

![Installation: Key](https://gitlab.com/sinclear/dcdp/raw/master/images/key_example.png)
